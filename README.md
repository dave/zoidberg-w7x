# Zoidberg-W7X: Flux-Coordinate Independent Grid Generator using W7X geometry for BOUT++

Zoidberg is a tool for generating grids for BOUT++ when using the
Flux-Coordinate Independent (FCI) method of evaluating derivative
parallel to a magnetic field.

This package provides routines specific for W7-X.
